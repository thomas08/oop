<?php

    require('Frog.php');
    require('Ape.php');
    
    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->name . "<br>";
    echo "legs : " . $sheep->kaki . "<br>";
    echo "cold blooded : " . $sheep->cold_blooded . "<br><br>";

    $kodok = new Frog ("buduk");
    echo "Name : " . $kodok->name . "<br>";
    echo "legs : " . $kodok->kaki . "<br>";
    echo "cold blooded : " . $kodok->cold_blooded . "<br>";
    echo "Jump : ";
    $kodok->jump();
    echo "<br><br>";
    
    $sungokong = new Ape ("buduk");
    echo "Name : " . $sungokong->name . "<br>";
    echo "legs : " . $sungokong->kaki . "<br>";
    echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
    echo "Yell : ";
    $sungokong->yell();
    echo "<br><br>";

?>